const
    parser = require('socket.io-msgpack-parser'),
    io = require('socket.io')(3000, {parser}),
    user = {},
    cache = {}


!function sync() {
    io.emit('sync:action', filter())
    setTimeout(sync, 30)
}()

io.on('connect', socket => {
    const id = socket.id

    socket
        .on('check:name', name => {
            for (const id in user) {
                if (user[id].name === name) {
                    return socket.emit('check:name:result', false)
                }
            }
            cache[id] = name
            socket.emit('check:name:result', true)
        })
        .on('sync:action', data => {
            user[id] ? user[id].action = data : null
        })
        .on('sync:state', data => {
            user[id] ? user[id].state = data : null
        })
        .on('disconnect', () => {
            socket.broadcast.emit('game:leave', id)
            delete user[id]
        })
        .on('game:join', skin => {
            user[id] = {
                skin,
                name: cache[id],
                action: {angle: 0, stop: true, shoot: false}
            }
            socket.broadcast.emit('game:join', id, skin)
            socket.emit('game:user', filter('state'))
            delete cache[id]
        })
})

function filter(field='action') {
    const data = {}
    for (const id in user) {
        data[id] = {
            skin: user[id].skin,
            ...user[id][field]
        }

    }

    return data
}

