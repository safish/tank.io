import io from 'socket.io-client'
import parser from 'socket.io-msgpack-parser'

import {monitor} from './core'
import config from './config'
import {prepare, entry, game} from './scenes'


new Promise((resolve, reject) => {
    config.socket = io('ws://192.168.6.10:3000', {parser, reconnection: false})
        .on('connect', resolve)
        .on('connect_error', reject)
}).then(prepare).then(() => {
    entry.show()
}).catch(err => alert(err.message))


monitor
    .on('scene:game', () => game.show())