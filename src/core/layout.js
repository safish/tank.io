const
    symbol = {
        horizontal: Symbol('horizontal'),
        vertical: Symbol('vertical'),
        grid: Symbol('grid'),
        listen: Symbol('listen'),
        place: Symbol('place')
    }

/*
* TIP:
* 在 layout 触发 added 之前
* 操作子元素，性能更好
*/

export default class Layout extends PIXI.Container {
    static GRID = 2
    static VERTICAL = 1
    static HORIZONTAL = 0

    constructor(option={}) {
        super()
        this.type = option.type || Layout.HORIZONTAL
        this.gap = getValue(option.gap, 10)
        this.column = getValue(option.column, 3)
        this.cell = option.cell
        this.misc = {
            x: 0,
            y: 0,
            max: 0,
            index: 0,
            delta: 0,
            added: false,
            rect: new PIXI.Rectangle()
        }

        this[symbol.listen]()
    }

    [symbol.listen]() {
        this.on('added', () => {
            this.misc.added = true
            this[symbol.place]()
            console.log('added layout')
        })

        this.on('removed', () => {
            this.misc.added = false
        })
    }

    [symbol.place]() {
        this.misc.x =
        this.misc.y =
        this.misc.max =
        this.misc.index =
        this.misc.delta = 0

        this.children.forEach((child, i) => {
            switch (this.type) {
                case Layout.GRID: {
                    this[symbol.grid](child, i)
                    break
                }

                case Layout.HORIZONTAL: {
                    this[symbol.horizontal](child, i)
                    break
                }

                case Layout.VERTICAL: {
                    this[symbol.vertical](child, i)
                    break
                }
            }
        })
    }

    addChild(...args) {
        super.addChild(...args)
        this.misc.added && this[symbol.place]()
    }

    [symbol.grid](child, i) {
        child.getLocalBounds(this.misc.rect)

        const
            ox = getValue(child.ox, -this.misc.rect.x / this.misc.rect.width),
            oy = getValue(child.oy, -this.misc.rect.y / this.misc.rect.height),
            ix = this.misc.index % this.column,
            iy = ~~(this.misc.index / this.column),
            w = this.cell && this.cell.width || child.width,
            h = this.cell && this.cell.height || child.height

        ix ? child.x = this.gap + this.misc.x + ox * w :
            child.x = ox * w


        this.misc.x = child.x + (1 - ox) * w

        iy ? child.y = this.gap + this.misc.y + oy * h : child.y = oy * h

        this.misc.max = Math.max(this.misc.max, child.y + (1 - oy) * h)

        ix === this.column - 1 ? this.misc.y = this.misc.max : null

        this.misc.index++
    }

    [symbol.horizontal](child, i) {
        child.getLocalBounds(this.misc.rect)
        const
            ox = getValue(child.ox, -this.misc.rect.x / this.misc.rect.width),
            w = this.cell && this.cell.width || child.width

        i ? child.x = this.gap + this.misc.delta + ox * w : child.x = ox * w
        this.misc.delta = child.x + (1 - ox) * w
    }

    [symbol.vertical](child, i) {
        child.getLocalBounds(this.misc.rect)

        const
            oy = getValue(child.ox, -this.misc.rect.y / this.misc.rect.height),
            h = this.cell && this.cell.height || child.height

        i ? child.y = this.gap + this.misc.delta + oy * h : child.y = oy * h
        this.misc.delta = child.y + (1 - oy) * h
    }

}

function getValue(v, s) {
    return v === undefined ? s : v
}