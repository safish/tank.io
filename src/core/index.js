import config from '../config'
import * as PIXI from 'pixi.js'

const
    mql = window.matchMedia('(orientation: portrait)'),
    app = new PIXI.Application({
        backgroundColor: config.bgColor,
        sharedLoader: true,
        antialias: true
    })


if (mql.matches && config.design.mode === 'landscape') {
    config.screen.width = window.innerHeight
    config.screen.height = window.innerWidth
    config.screen.resolution = window.devicePixelRatio
    app.view.style.top = `${(window.innerHeight - window.innerWidth) * .5}px`
    app.view.style.left = `${(window.innerWidth - window.innerHeight) * .5}px`
    app.view.style.width = `${window.innerHeight}px`
    app.view.style.height = `${window.innerWidth}px`
} else {
    config.screen.width = window.innerWidth
    config.screen.height = window.innerHeight
    config.screen.resolution = window.devicePixelRatio
}

app.renderer.resize(
    config.screen.width * window.devicePixelRatio,
    config.screen.height * window.devicePixelRatio
)

config.zoom.mix.push(
    app.screen.width / config.design.width,
    app.screen.height / config.design.height
)

document.body.appendChild(app.view)

// 设置节点相对屏幕中心的偏移
app.translate = (node, x=0, y=0) => {
    const rect = node.getBounds(false)
    node.position.set(
        (app.screen.width + rect.width) * .5 - rect.right + x,
        (app.screen.height + rect.height) * .5 - rect.bottom + y
    )
}

PIXI.interaction.InteractionManager.prototype.mapPositionToPoint = function(point, x, y) {
    let rect

    // IE 11 fix
    if (!this.interactionDOMElement.parentElement) {
        rect = {x: 0, y: 0, width: 0, height: 0}
    } else {
        rect = this.interactionDOMElement.getBoundingClientRect()
    }

    const resolutionMultiplier = navigator.isCocoonJS ? this.resolution : (1.0 / this.resolution)

    /*
    * 特殊处理: 强制横屏情况
    */
    if (mql.matches) {
        point.x = (y - rect.top) * (this.interactionDOMElement.width / rect.height) * resolutionMultiplier
        point.y = (1 - (x - rect.left) / rect.width) * this.interactionDOMElement.height * resolutionMultiplier
    } else {
        point.x = ((x - rect.left) * (this.interactionDOMElement.width / rect.width)) * resolutionMultiplier
        point.y = ((y - rect.top) * (this.interactionDOMElement.height / rect.height)) * resolutionMultiplier
    }
}

export default app
export {default as Layout} from './layout'
export {default as physics} from './physics'
export {default as Camera} from './camera'
export const monitor = new PIXI.utils.EventEmitter()
