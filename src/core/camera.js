
export default class Camera extends PIXI.Container {
    target = null
    offset = new PIXI.Point()
    targetPoint = new PIXI.Point()
    tween = {x: false, y: false}
    screen = null

    constructor(screen, tx, ty) {
        super()
        this.screen = screen
        this.tween.x = !!tx
        this.tween.y = !!ty
    }

    follow(target) {
        this.target = target
    }

    focus(t) {
        const
            dx = this.screen.width * .5 + this.offset.x,
            dy = this.screen.height * .5 + this.offset.y,
            damping = - Math.exp(-t / 10)

        this.target.getGlobalPosition(this.targetPoint)
        this.targetPoint.x = (dx - this.targetPoint.x) * (this.tween.x ? damping : 1)
        this.targetPoint.y = (dy - this.targetPoint.y) * (this.tween.y ? damping : 1)
        this.position.x += this.targetPoint.x
        this.position.y += this.targetPoint.y
    }

    update(t) {
        this.target && this.focus(t)
    }
}