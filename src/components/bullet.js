import {physics} from '../core'
import {toFixed} from '../utils'

const
    pool = [],
    bullets = [] // 激活的子弹

class Bullet extends PIXI.Sprite {
    shadow = {x: 0, y: 0, rotation: 0}
    velocity = new PIXI.Point()
    t = 0


    constructor() {
        super(PIXI.Texture.fromFrame('bullet.png'))
        this.anchor.set(.5)
        this.addBody({
            fixRotation: true,
            bullet: true
        }).addBox({
            width: this.width,
            height: this.height,
            isSensor: true
        })

        this.listen()
    }

    shoot(rotation, position) {
        const impulse = physics.Vec2(
            ~~(Math.sin(rotation) * 300),
            ~~(-Math.cos(rotation) * 300)
        )
        this.rotation = rotation
        this.position.copy(position)
        this.body.syncPosition()
        this.body.setAngle(rotation)
        this.body.applyLinearImpulse(
            impulse,
            this.body.getPosition(),
            true
        )
    }

    update() {
        this.x !== this.shadow.x ? this.x += ~~(this.velocity.x * .3) : null
        this.y !== this.shadow.y ? this.y += ~~(this.velocity.y * .3) : null

        this.x += ~~((this.x - this.shadow.x) * this.t * (this.t - 2))
        this.y += ~~((this.y - this.shadow.y) * this.t * (this.t - 2))

        this.t >= 1 ? this.t = 0 : this.t += .001
    }

    listen() {
        this
            .on('collide', () => {
                const index = bullets.findIndex(bullet => bullet === this)
                if (index !== -1) {
                    bullets.splice(index, 1)
                    pool.push(this)
                }
            })
    }
}

export default {
    recycle(bullet) {

    },

    shoot(angle, position) {
        const bullet = pool.pop() || new Bullet()
        bullet.shoot(angle, position)
        bullets.push(bullet)
        return bullet
    },

    update() {
        for (const bullet of bullets) {
            bullet.update()
        }
    },

    sync() {
        for (const bullet of bullets) {
            bullet.sync()
        }
    }
}