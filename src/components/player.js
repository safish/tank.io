import {toFixed} from '../utils'
import bullet from './bullet'

export default class Player extends PIXI.Sprite {
    velocity = new PIXI.Point()
    speed = 12
    toward = 0
    shadow = {x: 0, y: 0, rotation: 0}
    vector = new PIXI.Point()
    stamp = {
        shoot: 0,
        update: 0
    }

    constructor({skin, x=0, y=0, rotation=0}) {
        super(PIXI.Texture.fromFrame(`tank.${skin}.png`))
        this.rotation = rotation
        this.position.set(x, y)
        this.anchor.set(.5)

        this.addBody({
            angle: rotation,
            fixedRotation: true
        }).addBox({
            width: this.width,
            height: this.height
        })
    }

    sync(action) {
        if (action.stop) {
            this.velocity.set(0)
        } else {
            this.velocity.x = ~~(Math.cos(action.angle) * this.speed)
            this.velocity.y = ~~(Math.sin(action.angle) * this.speed)
            this.body.setAngle(toFixed(action.angle + Math.PI * .5, 2))
        }
        /*
        * 简单的处理
        * 避免精度问题导致的“不同步”
        */
        this.body.setLinearVelocity(this.velocity)
        action.shoot && this.shoot()
    }

    shoot() {
        const now = Date.now()
        if (now - this.stamp.shoot < 500) return
        this.stamp.shoot = now
        this.parent.addChild(bullet.shoot(this.rotation, this.position))
    }

    destroy() {
        super.destroy()
        this.body.destroy()
        this.body = null
    }

    update() {
        const t = this.stamp.update

        this.x !== this.shadow.x ? this.x += ~~(this.velocity.x * .3) : null
        this.y !== this.shadow.y ? this.y += ~~(this.velocity.y * .3) : null

        this.x += ~~((this.x - this.shadow.x) * t * (t - 2))
        this.y += ~~((this.y - this.shadow.y) * t * (t - 2))
        this.rotation = this.shadow.rotation

        t >= 1 ? this.stamp.update = 0 : this.stamp.update += .001
    }
}