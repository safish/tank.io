export function toFixed(i, precision=3) {
    return +i.toFixed(precision)
}