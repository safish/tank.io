export default {
    cdn     : '.',
    bgColor : 0xe0ddcc,

    zoom    : {
        mix: [],
        get min() {return Math.min(...this.mix)},
        get max() {return Math.max(...this.mix)}
    },
    socket  : null,
    screen  : {
        width: 750,
        height: 1334,
        mode: 'portrait',
        resolution: 1,
        get ratio() {return this.width / this.height}
    },
    design  : {
        width: 1334,
        height: 750,
        mode: 'landscape',
        get ratio() {return this.width / this.height}
    }
}
