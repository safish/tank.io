import core, {monitor, physics, Camera} from '../core'
import {Player, Gamepad, bullet} from '../components'
import config from '../config'


export default {
    players: {},
    pushTime: 0,

    setup() {
        this.container = new PIXI.Container()
        this.camera = new Camera(core.screen)
        this.gamepad = new Gamepad()
        this.container.addChild(this.camera, this.gamepad)
        config.socket.emit(
            'game:join',
            ['blue', 'dark', 'green', 'red', 'sand'][~~(Math.random() * 5)]
        )
    },

    listen() {
        this.container.on('added', () => {
            const rect = this.gamepad.getBounds(false)
            this.gamepad.position.set(
                (core.screen.width + rect.width) * .5 - rect.right,
                core.screen.height - 50 - rect.bottom
            )
        })

        config.socket
            .on('game:user', data => {
                for (const id in data) {
                    if (!(id in this.players)) {
                        const
                            state = data[id],
                            player = this.players[id] = new Player(state)

                        config.socket.id === id && this.camera.follow(player)
                        this.camera.addChild(player)
                    }
                }
            })
            .on('game:join', (id, skin) => {
                this.players[id] = new Player({skin})
                this.camera.addChild(this.players[id])
            })
            .on('game:leave', id => {
                const player = this.players[id]
                player && player.destroy()
                delete this.players[id]
            })
            .on('sync:action', data => {
                this.sync(data)
            })
    },

    sync(data) {
        for (const key in data) {
            const
                player = this.players[key],
                action = data[key]

            if (!player || !action) continue

            player.sync(action)
        }

        physics.sync()
    },

    update(t) {
        for (const key in this.players) {
            const player = this.players[key]
            player && player.update()
        }

        this.camera.update(t)
        bullet.update()
    },

    push() {
        this.pushTime++
        if (this.pushTime === 10 && this.players[config.socket.id]) {
            this.pushTime = 0
            config.socket.emit('sync:state', this.players[config.socket.id].shadow)
        }
        config.socket.emit('sync:action', this.gamepad.data)
        setTimeout(this.push.bind(this), 30)
    },

    show() {
        this.setup()
        this.listen()
        this.push()
        core.stage.addChild(this.container)
        core.ticker.add(this.update, this)
    },

    hide() {

    }
}