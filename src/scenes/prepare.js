import core from '../core'


export default function() {
    return new Promise(resolve => {
        core.loader
            .add('misc', 'static/textures/misc.json')
            .load(resolve)
    })
}