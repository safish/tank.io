import core, {monitor} from '../core'
import {tween, chain} from 'popmotion'
import config from '../config'

export default {
    tanks: ['red', 'green', 'blue', 'dark', 'sand'],

    setup() {
        this.container = new PIXI.Container()
        this.slogan = new PIXI.Text('TANK.IO', {
            fontFamily: "raleway-bold",
            fill: 0xffffff,
            dropShadow: true,
            dropShadowBlur: 20,
            dropShadowAngle: .08,
            fill: [0xffffff, 0xc8fff7],
            fontSize: 120
        })

        this.btn = {
            start: PIXI.Sprite.from('btn.start.png')
        }

        this.btn.start.visible = false
        this.btn.start.anchor.set(.5)
        this.btn.start.scale.set(2)

        this.slogan.anchor.set(.5)
        this.slogan.scale.set(0)

        this.container.addChild(this.slogan, this.btn.start)
    },

    addInput() {
        const
            input = document.createElement('input'),
            shadow = document.createElement('form')

        input.type = 'text'
        input.placeholder = '请输入昵称'
        input.autofocus = true

        input.classList.add('nickname')
        shadow.classList.add('shadow')

        shadow.appendChild(input)
        document.body.appendChild(shadow)

        shadow.addEventListener('submit', ev => {
            ev.preventDefault()
            config.socket.emit('check:name', input.value)
        })

        return {
            destroy() {
                shadow.parentElement.removeChild(shadow)
            }
        }
    },

    animate() {
        chain(
            tween({
                from: {
                    rotation: 0,
                    scale: 0
                },

                to: {
                    rotation: Math.PI * 2,
                    scale: 1
                },

                duration: 5e2
            }),
            tween({
                from: this.slogan.y,
                to: this.slogan.y - 100,
                duration: 3e2
            })
        ).start({
            update: v => {
                if (v.hasOwnProperty('scale')) {
                    this.slogan.rotation = v.rotation
                    this.slogan.scale.set(v.scale)
                } else {
                    this.slogan.y = v
                }
            },

            complete: () => {
                this.btn.start.visible = true
                core.translate(this.btn.start, 0, 100)
            }
        })
    },

    listen() {
        this.container.on('added', () => {
            core.translate(this.slogan)
            this.animate()
        })
        this.btn.start.interactive = true
        this.btn.start.on('pointerdown', () => {
            if (this.btn.start.tweening) return
            this.btn.start.tweening = true
            tween({
                from: this.btn.start.scale.x,
                to: this.btn.start.scale.x + .2,
                duration: 2e2,
                yoyo: 1
            }).start({
                update: v => this.btn.start.scale.set(v),
                complete: () => {
                    this.btn.start.tweening = false
                    this.input = this.addInput()
                }
            })
        })

        config.socket
            .on('check:name:result', async result => {
                if (result) {
                    this.input.destroy()
                    await this.hide()
                    monitor.emit('scene:game')
                } else alert('昵称被占用！')
            })
    },

    show() {
        this.setup()
        this.listen()
        core.stage.addChild(this.container)
    },

    hide() {
        return new Promise(resolve => {
            tween({
                from: 1,
                to: 0,
                duration: 2e2
            }).start({
                update: v => this.container.alpha = v,
                complete: () => {
                    this.container.destroy()
                    resolve()
                }
            })
        })

    }
}